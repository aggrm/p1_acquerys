/*  
    Crea una aplicación Java que conecte con la base de datos.
    3.- Modificar la tabla álbum para incluir un nuevo campo que contenga las imágenes de las carátulas de cada álbum.
    4.- Consultar con consultas SELECT las tablas álbum  y cancion. Las consultas deben ser parametrizadas (Statement) y no parametrizadas (preparedStatement). El resultado debe mostrarse en forma de lista de resultados.
    5.- Inserciones de datos en las tablas canciones y álbum de manera atómica. Si falla la inserción en una de las tablas entonces todo el proceso se debe anular.
*/
package src;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author xp
 */
public class GestorConexion 
{
    Connection connection;
    
    public GestorConexion() 
    {
        connection = null;
        try 
        {
            String url1 = "jdbc:mysql://localhost:3306/discografica?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
            String user = "root";
            //String password = "";
            String password = "root";
            connection = (Connection)DriverManager.getConnection(url1, user, password);
            if (connection != null) 
            {
                System.out.println("Conectado a discográfica…");
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("ERROR: dirección o usuario/clave no válida");
            ex.printStackTrace();
        }
    }
    
    public void cerrar_conexion() 
    {
        try 
        {
            System.out.println("Cerrada conexion con la BBDD");
            connection.close();
        } 
        catch (SQLException ex) 
        {
            System.out.println("ERROR:al cerrar la conexión");
            ex.printStackTrace();
        }
    }
    
    public int addColumna(String nombreColumna)
    {
        try 
        {
            Statement sta = connection.createStatement();
            sta.executeUpdate("ALTER TABLE album ADD `" + nombreColumna + "` VARCHAR(50) DEFAULT NULL;");
            System.out.println("Columna añadida");
            sta.close();
            return 0;
        } 
        catch (SQLException ex) 
        {
            ex.printStackTrace();
            System.out.println("ERROR");
            return -1;
        }
    }
    
    public ResultSet buscaTabla(String tabla)
    {
        ResultSet rs = null;
        try 
        {
            Statement sta = connection.createStatement();
            String query = "SELECT * FROM " + tabla;
            rs = sta.executeQuery(query);
//            rs.close();
//            sta.close();
            return rs;
        } 
        catch (SQLException ex) 
        {
            System.out.println("ERROR:al consultar");
            ex.printStackTrace();
            return rs;
        }
    }
    public int insert_album_commit(String id, String titulo, String tipo, String anno, String caratula)
    {
        try
        {
            connection.setAutoCommit(false);
            Statement sta = connection.createStatement();
            sta.executeUpdate("INSERT INTO album (ID, Titulo, Tipo, anno_publicacion, Caratula) VALUES ('" + id + "', '" + titulo + "', '" + tipo + "', '" + anno + "', '" + caratula + "')");
            connection.commit();
            return 0;
        } 
        catch (SQLException ex) 
        {
            System.out.println("ERROR: al hacer un Insert");
            try
            {
                if(connection!=null)
                {
                    connection.rollback();
                }
            }
            catch(SQLException se2)
            {
                se2.printStackTrace();
            }
            ex.printStackTrace();
            return -1;
        }
    }
    
    public int insert_cancion_commit(String id, String titulo, String duracion, String letras, String id_album)
    {
        try
        {
            connection.setAutoCommit(false);
            Statement sta = connection.createStatement();
            sta.executeUpdate("INSERT INTO cancion (ID, Titulo, Duracion, Letras, Id_album) VALUES ('" + id + "', '" + titulo + "', '" + duracion + "', '" + letras + "', '" + id_album + "')");
            connection.commit();
            return 0;
        } 
        catch (SQLException ex) 
        {
            System.out.println("ERROR:al hacer un Insert");
            try
            {
                if(connection!=null)
                {
                    connection.rollback();
                }
            }
            catch(SQLException se2)
            {
                se2.printStackTrace();
            }
            ex.printStackTrace();
            return -1;
        }
    }
    public ResultSet buscadorPalabras(String tablaSeleccionada, String palabraBuscada)
    {
        ResultSet rs = null;
        try 
        {
            String query = "SELECT * FROM " + tablaSeleccionada + " WHERE titulo LIKE ?";
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, palabraBuscada);
            rs = pst.executeQuery();
            return rs;
        } 
        catch (SQLException ex) 
        {
            System.out.println("ERROR:al consultar");
            ex.printStackTrace();
            return rs;
        }

    }
}